package com.bot.demo.CarouselPilihMenu.MenuUtama;

import com.bot.demo.TestUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.linecorp.bot.model.message.FlexMessage;
import lombok.SneakyThrows;
import org.assertj.core.api.Assertions;
import org.json.JSONException;
import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class CarouselPilihUtamaDagingTest {

    ObjectMapper objectMapper;

    @Before
    public void setUp(){
        objectMapper = TestUtil.objectMapperWithProductionConfiguration(false);
    }

    @Test
    public void flexmessage() throws IOException, JSONException, ParseException, URISyntaxException {
        final FlexMessage flexMessage = new CarouselPilihUtamaDaging().get();
        test(flexMessage);
    }

    void test(final Object original) throws IOException {
        final Object reconstructed = serializeThenDeserialize(original);
        Assertions.assertThat(reconstructed).isEqualTo(original);
    }

    @SneakyThrows
    Object serializeThenDeserialize(final Object original)
            throws IOException {
        final String asJson = objectMapper.writeValueAsString(original);
        final Object reconstructed = objectMapper.readValue(asJson, original.getClass());

        return reconstructed;
    }
}
