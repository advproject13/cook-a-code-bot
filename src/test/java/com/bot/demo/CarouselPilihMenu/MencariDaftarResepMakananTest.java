package com.bot.demo.CarouselPilihMenu;


import org.json.JSONException;
import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class MencariDaftarResepMakananTest {


    @Test
    public void TestcariDenganAPI() throws URISyntaxException, JSONException, ParseException {
        ArrayList<String[]> testHasil = new MencariDaftarResepMakanan().CariDenganAPI("Chicken");

        Assert.assertFalse(testHasil.isEmpty());

        RestTemplate restTemplate = new RestTemplate();
        String spoonAcularFormat = "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/food/menuItems/suggest?number=10&" + "query=%s";
        String bahanMakananan = "Soup";
        String AcularQuery = String.format(spoonAcularFormat, bahanMakananan);


        URI uri = new URI(AcularQuery);

        HttpHeaders headers = new HttpHeaders();
        headers.set("X-RapidAPI-Host", "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com");
        headers.set("X-RapidAPI-Key", "f812552b29msha701e7fb5b69a14p11b1bfjsnaeb28c3581d5");


        HttpEntity requestEntity = new HttpEntity(null, headers);

        ResponseEntity<String> responseResult = restTemplate.exchange(uri, HttpMethod.GET, requestEntity, String.class);

        Assert.assertEquals(200,responseResult.getStatusCodeValue());
        Assert.assertEquals(true, responseResult.getBody().contains("results"));

    }
}
