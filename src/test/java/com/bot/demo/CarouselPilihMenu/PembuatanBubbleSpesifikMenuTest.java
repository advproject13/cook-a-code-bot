package com.bot.demo.CarouselPilihMenu;

import com.bot.demo.TestUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.linecorp.bot.model.message.flex.container.Bubble;
import lombok.SneakyThrows;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class PembuatanBubbleSpesifikMenuTest{
    ObjectMapper objectMapper;

    @Before
    public void setUp() throws Exception {
        objectMapper = TestUtil.objectMapperWithProductionConfiguration(false);
    }

    @Test
    public void flexmessage() throws JsonProcessingException, IOException {
        final Bubble flexMessage = new PembuatanBubbleSpesifikMenu().createBubble("https://i.ibb.co/XYsbLmP/photo-1455619452474-d2be8b1e70cd.jpg",
                "Opor Ayam",
                "Pilih es krim yan kamu inginkan...",
                new String[] {"Chicken", "Snack", "ice cream", "snack", "pudding"});
        test(flexMessage);
    }

    void test(final Object original) throws JsonProcessingException, IOException {
        final Object reconstructed = serializeThenDeserialize(original);
        Assertions.assertThat(reconstructed).isEqualTo(original);
    }

    @SneakyThrows
    Object serializeThenDeserialize(final Object original)
            throws JsonProcessingException, IOException {
        final String asJson = objectMapper.writeValueAsString(original);
        final Object reconstructed = objectMapper.readValue(asJson, original.getClass());

        return reconstructed;
    }
}