package com.bot.demo;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.GroupSource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Instant;

@RunWith(MockitoJUnitRunner.class)
public class CookACodeControllerTests {

    @Mock
    private LineMessagingClient lineMessagingClient;
    @InjectMocks
    private CookACodeController underTest;

    @Test
    public void handleTextMessageEvent() throws Exception {
        // if group user send ".help" as text message
        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id", "bye"),
                Instant.now()
        );

//        // mock line bot api client response
//        when(lineMessagingClient.replyMessage(new ReplyMessage(
//                "replyToken", singletonList(new TextMessage("Leaving group"))
//        ))).thenReturn(CompletableFuture.completedFuture(
//                new BotApiResponse("ok", Collections.emptyList())
//        ));
//        when(lineMessagingClient.leaveGroup("groupId"))
//                .thenReturn(CompletableFuture.completedFuture(
//                        new BotApiResponse("ok", Collections.emptyList())
//                ));
//
//        underTest.handleTextMessageEvent(request);
//
//        // confirm replyMessage is called with following parameter
////        verify(lineMessagingClient).replyMessage(new ReplyMessage(
////                "replyToken", singletonList(new TextMessage("Leaving group"))
//        // leave group api must be called
//        verify(lineMessagingClient).leaveGroup("groupId");
    }
}
