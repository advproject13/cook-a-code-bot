package com.bot.demo;

import java.io.IOException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.linecorp.bot.model.message.Message;
import lombok.SneakyThrows;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

public class MenuFavoritControllerTest {
    ObjectMapper objectMapper;
    MenuFavoritController menuFavoritController;

    @Before
    public void setUp() throws Exception {
        objectMapper = TestUtil.objectMapperWithProductionConfiguration(false);
        menuFavoritController = new MenuFavoritController("test");
    }

    @Test
    public void message() throws JsonProcessingException, IOException {
        final Message getFavoritTest = new MenuFavoritController("test").get();
        test(getFavoritTest);
    }

    void test(final Object original) throws JsonProcessingException, IOException {
        final Object reconstructed = serializeThenDeserialize(original);
        Assertions.assertThat(reconstructed).isEqualTo(original);
    }

    @Test
    public void testTambahResepBelumTerdaftar() throws Exception {
        final String output = menuFavoritController.tambahFavorit("test");
        Assertions.assertThat(output).isEqualTo("");
    }

    @Test
    public void testTambahResepSudahTerdaftar() throws Exception {
        final String output = menuFavoritController.tambahFavorit("test");
        Assertions.assertThat(output).isEqualTo("");
    }

    @Test
    public void testHapusResepSudahTerdaftar() throws Exception {
        final String output = menuFavoritController.hapusFavorit("test");
        Assertions.assertThat(output).isEqualTo("");
    }

    @Test
    public void testHapusResepBelumTerdaftar() throws Exception {
        final String output = menuFavoritController.hapusFavorit("test");
        Assertions.assertThat(output).isEqualTo("");
    }

    /*@Test
    public void testTambahResepBelumTerdaftar throws Exception {
        final String output = menuFavoritController.tambahFavorit("test");
        Assertions.assertThat(output).isEqualTo("test sukses ditambahkan ke dalam daftar favoritmu!");
    }

    @Test
    public void testTambahResepSudahTerdaftar throws Exception {
        final String output = menuFavoritController.tambahFavorit("test");
        Assertions.assertThat(output).isEqualTo("test sudah terdaftar di dalam daftar favoritmu.");
    }

    @Test
    public void testHapusResepSudahTerdaftar throws Exception {
        final String output = menuFavoritController.hapusFavorit("test");
        Assertions.assertThat(output).isEqualTo("test sukses dihapus dari daftar favoritmu.");
    }

    @Test
    public void testHapusResepBelumTerdaftar throws Exception {
        final String output = menuFavoritController.hapusFavorit("test");
        Assertions.assertThat(output).isEqualTo("test tidak ditemukan dari daftar favoritmu.");
    }*/


    @SneakyThrows
    Object serializeThenDeserialize(final Object original)
            throws JsonProcessingException, IOException {
        final String asJson = objectMapper.writeValueAsString(original);
        final Object reconstructed = objectMapper.readValue(asJson, original.getClass());

        return reconstructed;
    }
}
