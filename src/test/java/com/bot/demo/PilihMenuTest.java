package com.bot.demo;

import com.linecorp.bot.model.message.FlexMessage;
import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.junit.Test;

import java.net.URISyntaxException;

public class PilihMenuTest {

    @Test
    public void test() throws ParseException, URISyntaxException {
        FlexMessage menuPembuka = new PilihMenu().pembuka("Kue");
        FlexMessage menuUtama = new PilihMenu().utama("Daging");
        FlexMessage menuPenutup = new PilihMenu().penutup("Buah");


        Assert.assertFalse(menuPembuka.toString(), Boolean.parseBoolean(" "));
        Assert.assertFalse(menuUtama.toString(), Boolean.parseBoolean(" "));
        Assert.assertFalse(menuPenutup.toString(), Boolean.parseBoolean(" "));
    }

}
