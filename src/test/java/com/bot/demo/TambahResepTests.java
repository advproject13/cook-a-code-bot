package com.bot.demo;

import java.io.IOException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.linecorp.bot.model.message.Message;
import lombok.SneakyThrows;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

public class TambahResepTests {
    ObjectMapper objectMapper;

    @Before
    public void setUp() throws Exception {
        objectMapper = TestUtil.objectMapperWithProductionConfiguration(false);
    }

    @Test
    public void message() throws JsonProcessingException, IOException {
        final Message tambahresep = new TambahResep().get();
        test(tambahresep);
    }

    void test(final Object original) throws JsonProcessingException, IOException {
        final Object reconstructed = serializeThenDeserialize(original);
        Assertions.assertThat(reconstructed).isEqualTo(original);
    }

    @SneakyThrows
    Object serializeThenDeserialize(final Object original)
            throws JsonProcessingException, IOException {
        final String asJson = objectMapper.writeValueAsString(original);
        final Object reconstructed = objectMapper.readValue(asJson, original.getClass());

        return reconstructed;
    }
}
