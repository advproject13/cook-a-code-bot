package com.bot.demo;

import com.bot.demo.CarouselPilihMenu.MenuPembuka.*;
import com.bot.demo.CarouselPilihMenu.MenuPenutup.*;
import com.bot.demo.CarouselPilihMenu.MenuUtama.*;
import com.linecorp.bot.model.message.FlexMessage;
import org.json.JSONException;
import org.json.simple.parser.ParseException;

import java.net.URISyntaxException;

public class PilihMenu {
    FlexMessage flexMessage;

    public FlexMessage pembuka(String user_input) throws JSONException, ParseException, URISyntaxException {
        if(user_input.equals("Kue")) {
            flexMessage = new CarouselPilihPembukaKue().get();
        }
        else if(user_input.equals("Es Krim")) {
            flexMessage = new CarouselPilihPembukaEsKrim().get();
        }
        else if(user_input.equals("Puding")) {
            flexMessage = new CarouselPilihPembukaPuding().get();
        }
        return flexMessage;
    }

    public FlexMessage utama(String user_input) throws JSONException, ParseException, URISyntaxException {
        if(user_input.equals("Ayam")) {
            flexMessage = new CarouselPilihUtamaAyam().get();
        }
        else if(user_input.equals("Ikan")) {
            flexMessage = new CarouselPilihUtamaIkan().get();
        }
        else if(user_input.equals("Daging")) {
            flexMessage = new CarouselPilihUtamaDaging().get();
        }
        return flexMessage;
    }

    public FlexMessage penutup(String user_input) throws JSONException, ParseException, URISyntaxException {
        if(user_input.equals("Buah")) {
            flexMessage = new CarouselPilihPenutupBuah().get();
        }
        else if(user_input.equals("Sayur")) {
            flexMessage = new CarouselPilihPenutupSayur().get();
        }
        else if(user_input.equals("Sup")) {
            flexMessage = new CarouselPilihPenutupSup().get();
        }
        return flexMessage;
    }
}
