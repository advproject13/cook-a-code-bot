package com.bot.demo;

import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class TampilkanDetailPilihMenu {

    private String spoonAcularFormat = "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/%s" + "/information";

    public String CariDetailMakanDenganAPI (String id) throws JSONException, org.json.simple.parser.ParseException, URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();
        String AcularQuery = String.format(spoonAcularFormat, id);
        URI linkAPI = new URI(AcularQuery);

        HttpHeaders headers = new HttpHeaders();
        headers.set("X-RapidAPI-Host", "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com");
        headers.set("X-RapidAPI-Key", "f812552b29msha701e7fb5b69a14p11b1bfjsnaeb28c3581d5");


        HttpEntity requestEntity = new HttpEntity(null, headers);

        ResponseEntity<String> responseResult = restTemplate.exchange(linkAPI, HttpMethod.GET, requestEntity, String.class);

        JSONParser jsonParser = new JSONParser();
        JSONObject dataMenu = (JSONObject) jsonParser.parse(responseResult.getBody());

        JSONArray daftarResep = (JSONArray) dataMenu.get("extendedIngredients");
        String ingredients = " ";
        for(Object resep : daftarResep){
            JSONObject menu = (JSONObject) resep;
            String nama_resep = (String) menu.get("name");
            ingredients = ingredients + nama_resep + ", ";
        }

        String daftarIntrusksi = (String) dataMenu.get("instructions");
        String[] detailInstruksi = daftarIntrusksi.split(".");
        String stepfull = "";
        for(Object instruksi : detailInstruksi){
            stepfull = stepfull + instruksi + "\n";
        }
        return (ingredients + "\n" + stepfull);
    }
}
