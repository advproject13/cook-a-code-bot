package com.bot.demo;

import org.springframework.data.repository.CrudRepository;
import com.bot.demo.Menu;
import com.bot.demo.MenuFavorit;
import java.util.List;
import java.util.Optional;

public interface MenuFavoritRepository extends CrudRepository<MenuFavorit, Long> {
    Optional<MenuFavorit> findByUserId(String userId);

    Optional<MenuFavorit> findByNamaResep(String namaResep);

    Optional<MenuFavorit> findByUserIdAndNamaResep(String userId, String namaResep);

    List<MenuFavorit> findAllByUserId(String userId);

    List<MenuFavorit> findAll();

    Menu findAllByNamaResep(String namaResep);
}
