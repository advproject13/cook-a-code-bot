package com.bot.demo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class MenuFavorit {
    @Id
    @Column(nullable = false)
    private String userId;
    @Column(nullable = false)
    private String namaResep;

    public MenuFavorit() {
    }

    public MenuFavorit(String userId, String namaResep){
        this.userId = userId;
        this.namaResep = namaResep;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setNamaResep(String namaResep) {
        this.namaResep = namaResep;
    }

    public String getNamaResep() {
        return namaResep;
    }
}
