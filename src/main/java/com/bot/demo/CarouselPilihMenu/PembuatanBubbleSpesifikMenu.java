package com.bot.demo.CarouselPilihMenu;

import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.message.flex.component.*;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.*;

import static java.util.Arrays.asList;

public class PembuatanBubbleSpesifikMenu {
    public Bubble createBubble(String url, String namaResep, String desc, String[] buttonDaftarMenu){
        final Image heroBlock =
                Image.builder()
                        .url(url)
                        .size(Image.ImageSize.FULL_WIDTH)
                        .aspectRatio(Image.ImageAspectRatio.R20TO13)
                        .aspectMode(Image.ImageAspectMode.Cover)
                        .build();

        final Box bodyBlock = createBodyBlock(namaResep, desc);
        final Box footerBlock = createFooterBlock(namaResep, buttonDaftarMenu);

        return Bubble.builder()
                .hero(heroBlock)
                .body(bodyBlock)
                .footer(footerBlock)
                .build();
    }

    private Box createBodyBlock(String namaResep, String desc) {
        final Text title =
                Text.builder()
                        .text(namaResep)
                        .weight(Text.TextWeight.BOLD)
                        .size(FlexFontSize.XL)
                        .build();
        final Text description =
                Text.builder()
                        .text("Deskripsi")
                        .flex(3)
                        .size(FlexFontSize.SM)
                        .color("#AAAAAA")
                        .build();

        final Text customDesc =
                Text.builder()
                        .text(desc)
                        .size(FlexFontSize.SM)
                        .wrap(true)
                        .flex(5)
                        .gravity(FlexGravity.CENTER)
                        .color("#666666")
                        .build();

        final Box boxDesc = Box.builder()
                .layout(FlexLayout.BASELINE)
                .spacing(FlexMarginSize.SM)
                .contents(asList(description, customDesc))
                .build();

        final Box boxInside = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.SM)
                .margin(FlexMarginSize.LG)
                .contents(asList(boxDesc))
                .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(asList(title,
                        boxInside))
                .build();
    }

    private Box createFooterBlock(String namaResep, String[] daftarMenu) {
        final Separator separator = Separator.builder().build();

        final Button pilihMenu1 = Button
                .builder()
                .style(Button.ButtonStyle.LINK)
                .height(Button.ButtonHeight.SMALL)
                .action(new MessageAction(daftarMenu[0],".Detail" + " " +  daftarMenu[0]))
                .build();

        final Button pilihMenu2 =
                Button.builder()
                        .style(Button.ButtonStyle.LINK)
                        .height(Button.ButtonHeight.SMALL)
                        .action(new MessageAction(daftarMenu[1],".Detail" + " " + daftarMenu[1]))
                        .build();

        final Button pilihMenu3 =
                Button.builder()
                        .style(Button.ButtonStyle.LINK)
                        .height(Button.ButtonHeight.SMALL)
                        .action(new MessageAction(daftarMenu[2],".Detail" + " " +daftarMenu[2]))
                        .build();

        final Button pilihMenu4 =
                Button.builder()
                        .style(Button.ButtonStyle.LINK)
                        .height(Button.ButtonHeight.SMALL)
                        .action(new MessageAction(daftarMenu[3],".Detail" + " " +daftarMenu[3]))
                        .build();

        final Button pilihMenu5 =
                Button.builder()
                        .style(Button.ButtonStyle.LINK)
                        .height(Button.ButtonHeight.SMALL)
                        .action(new MessageAction(daftarMenu[4],".Detail" + " " +daftarMenu[4]))
                        .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.SM)
                .flex(1)
                .contents(asList(
                        separator,
                        pilihMenu1,
                        pilihMenu2,
                        pilihMenu3,
                        pilihMenu4,
                        pilihMenu5))
                .build();
    }
}
