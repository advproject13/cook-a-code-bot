package com.bot.demo.CarouselPilihMenu.MenuPenutup;

import com.bot.demo.CarouselPilihMenu.MencariDaftarResepMakanan;
import com.bot.demo.CarouselPilihMenu.PembuatanBubbleSpesifikMenu;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Carousel;
import org.json.JSONException;
import org.json.simple.parser.ParseException;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.function.Supplier;

import static java.util.Arrays.asList;

public class CarouselPilihPenutupBuah implements Supplier<FlexMessage> {
    String[] MenuFruitdiTampilkan = new String[5];

    public CarouselPilihPenutupBuah() throws JSONException, ParseException, URISyntaxException {
        ArrayList<String[]> daftarMenuBuah = new MencariDaftarResepMakanan().CariDenganAPI("Fruit");

        int daftarMenu = 4;
        for (int loop = 0; loop <= daftarMenu; loop++) {
            MenuFruitdiTampilkan[loop] = daftarMenuBuah.get(loop)[0] + " " + daftarMenuBuah.get(loop)[1];
        }
    }

    @Override
    public FlexMessage get() {

        Bubble Buah = new PembuatanBubbleSpesifikMenu().createBubble(
                "https://i.ibb.co/Db8JNMW/heather-barnes-1443935-unsplash.jpg",
                "Buah",
                "Pilih menu dengan buah yang kamu inginkan...",
                MenuFruitdiTampilkan
        );

        final Carousel carousel =
                Carousel.builder().contents(asList(Buah)).build();
        return new FlexMessage(".MenuBuah", carousel);
    }
}
