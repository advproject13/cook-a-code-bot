package com.bot.demo.CarouselPilihMenu.MenuPenutup;

import com.bot.demo.CarouselPilihMenu.MencariDaftarResepMakanan;
import com.bot.demo.CarouselPilihMenu.PembuatanBubbleSpesifikMenu;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Carousel;
import org.json.JSONException;
import org.json.simple.parser.ParseException;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.function.Supplier;

import static java.util.Arrays.asList;

public class CarouselPilihPenutupSup implements Supplier<FlexMessage> {
    String[] MenuSoupdiTampilkan = new String[5];

    public CarouselPilihPenutupSup() throws JSONException, ParseException, URISyntaxException {
        ArrayList<String[]> daftarMenuSup = new MencariDaftarResepMakanan().CariDenganAPI("Soup");

        int daftarMenu = 4;
        for (int loop = 0; loop <= daftarMenu; loop++) {
            MenuSoupdiTampilkan[loop] = daftarMenuSup.get(loop)[0] + " " + daftarMenuSup.get(loop)[1];
        }
    }

    @Override
    public FlexMessage get() {

        Bubble Sup = new PembuatanBubbleSpesifikMenu().createBubble(
                "https://i.ibb.co/9YhkDdX/monika-grabkowska-444160-unsplash.jpg",
                "Sup",
                "Pilih menu dengan sup yang kamu inginkan...",
                MenuSoupdiTampilkan
        );

        final Carousel carousel =
                Carousel.builder().contents(asList(Sup)).build();
        return new FlexMessage(".MenuSup", carousel);
    }
}
