package com.bot.demo.CarouselPilihMenu.MenuPenutup;

import com.bot.demo.CarouselPilihMenu.MencariDaftarResepMakanan;
import com.bot.demo.CarouselPilihMenu.PembuatanBubbleSpesifikMenu;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Carousel;
import org.json.JSONException;
import org.json.simple.parser.ParseException;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.function.Supplier;

import static java.util.Arrays.asList;

public class CarouselPilihPenutupSayur implements Supplier<FlexMessage> {
    String[] MenuVegetablesdiTampilkan = new String[5];

    public CarouselPilihPenutupSayur() throws JSONException, ParseException, URISyntaxException {
        ArrayList<String[]> daftarMenuSayur = new MencariDaftarResepMakanan().CariDenganAPI("Vegetables");

        int daftarMenu = 4;
        for (int loop = 0; loop <= daftarMenu; loop++) {
            MenuVegetablesdiTampilkan[loop] = daftarMenuSayur.get(loop)[0] + " " + daftarMenuSayur.get(loop)[1];
        }
    }

    @Override
    public FlexMessage get() {

        Bubble Sayur = new PembuatanBubbleSpesifikMenu().createBubble(
                "https://i.ibb.co/dGKYz61/scott-warman-525481-unsplash.jpg",
                "Sayur",
                "Pilih menu dengan sayur yang kamu inginkan...",
                MenuVegetablesdiTampilkan
        );

        final Carousel carousel =
                Carousel.builder().contents(asList(Sayur)).build();
        return new FlexMessage(".MenuSayur", carousel);
    }
}
