package com.bot.demo.CarouselPilihMenu.MenuPembuka;

import com.bot.demo.CarouselPilihMenu.MencariDaftarResepMakanan;
import com.bot.demo.CarouselPilihMenu.PembuatanBubbleSpesifikMenu;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Carousel;
import org.json.JSONException;
import org.json.simple.parser.ParseException;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.function.Supplier;

import static java.util.Arrays.asList;

public class CarouselPilihPembukaPuding implements Supplier<FlexMessage> {


    String[] MenuPuddingdiTampilkan = new String[5];

    public CarouselPilihPembukaPuding() throws JSONException, ParseException, URISyntaxException {
        ArrayList<String[]> daftarMenuPuding = new MencariDaftarResepMakanan().CariDenganAPI("Pudding");
        
        for (int loop = 0; loop <= 4; loop++) {
            MenuPuddingdiTampilkan[loop] = daftarMenuPuding.get(loop)[0] + " " + daftarMenuPuding.get(loop)[1];
        }

        this.get();
    }

    @Override
    public FlexMessage get() {

        Bubble Puding = new PembuatanBubbleSpesifikMenu().createBubble(
                "https://i.ibb.co/K58F49m/karly-gomez-216611-unsplash.jpg",
                "Puding",
                "Pilih menu dengan puding yang kamu inginkan...",
                MenuPuddingdiTampilkan
        );

        final Carousel carousel =
                Carousel.builder().contents(asList(Puding)).build();
        return new FlexMessage(".MenuPuding", carousel);
    }
}
