package com.bot.demo.CarouselPilihMenu.MenuPembuka;

import com.bot.demo.CarouselPilihMenu.MencariDaftarResepMakanan;
import com.bot.demo.CarouselPilihMenu.PembuatanBubbleSpesifikMenu;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Carousel;
import org.json.JSONException;
import org.json.simple.parser.ParseException;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.function.Supplier;

import static java.util.Arrays.asList;

public class CarouselPilihPembukaEsKrim implements Supplier<FlexMessage> {


    String[] menuIceCreamdiTampilkan = new String[5];

    public CarouselPilihPembukaEsKrim() throws JSONException, ParseException, URISyntaxException {
        ArrayList<String[]> daftarMenuIceCream = new MencariDaftarResepMakanan().CariDenganAPI("Ice+Cream");

        for (int loop = 0; loop <= 4; loop++) {
            menuIceCreamdiTampilkan[loop] = daftarMenuIceCream.get(loop)[0] + " " + daftarMenuIceCream.get(loop)[1];
        }
    }

    @Override
    public FlexMessage get() {

        Bubble EsKrim = new PembuatanBubbleSpesifikMenu().createBubble(
                "https://i.ibb.co/4dGvPmX/dan-gold-292708-unsplash.jpg",
                "Es Krim",
                "Pilih menu dengan es krim yang kamu inginkan...",
                menuIceCreamdiTampilkan
        );

        final Carousel carousel =
                Carousel.builder().contents(asList(EsKrim)).build();
        return new FlexMessage(".MenuEsKrim", carousel);
    }
}
