package com.bot.demo.CarouselPilihMenu.MenuPembuka;

import com.bot.demo.CarouselPilihMenu.MencariDaftarResepMakanan;
import com.bot.demo.CarouselPilihMenu.PembuatanBubbleSpesifikMenu;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Carousel;
import org.json.JSONException;
import org.json.simple.parser.ParseException;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.function.Supplier;

import static java.util.Arrays.asList;

public class CarouselPilihPembukaKue implements Supplier<FlexMessage> {


    String[] MenuCakediTampilkan = new String[5];

    public CarouselPilihPembukaKue() throws JSONException, ParseException, URISyntaxException {
        ArrayList<String[]> daftarMenuKue = new MencariDaftarResepMakanan().CariDenganAPI("Cake");

        for (int loop = 0; loop <= 4; loop++) {
            MenuCakediTampilkan[loop] = daftarMenuKue.get(loop)[0] + " " + daftarMenuKue.get(loop)[1];
        }
    }

    @Override
    public FlexMessage get() {

        Bubble Kue = new PembuatanBubbleSpesifikMenu().createBubble(
                "https://i.ibb.co/DC1zQ67/mike-meeks-1380795-unsplash.jpg",
                "Kue",
                "Pilih menu dengan Kue yang kamu inginkan...",
                MenuCakediTampilkan
        );

        final Carousel carousel =
                Carousel.builder().contents(asList(Kue)).build();
        return new FlexMessage(".MenuKue", carousel);
    }
}
