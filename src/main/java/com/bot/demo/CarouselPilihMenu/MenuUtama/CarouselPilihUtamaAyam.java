package com.bot.demo.CarouselPilihMenu.MenuUtama;

import com.bot.demo.CarouselPilihMenu.MencariDaftarResepMakanan;
import com.bot.demo.CarouselPilihMenu.PembuatanBubbleSpesifikMenu;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Carousel;
import org.json.JSONException;
import org.json.simple.parser.ParseException;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.function.Supplier;

import static java.util.Arrays.asList;

public class CarouselPilihUtamaAyam implements Supplier<FlexMessage> {
    String[] MenuChickendiTampilkan = new String[5];

    public CarouselPilihUtamaAyam() throws JSONException, ParseException, URISyntaxException {
        ArrayList<String[]> daftarMenuAyam = new MencariDaftarResepMakanan().CariDenganAPI("Chicken");

        int daftarMenu = 4;
        for (int loop = 0; loop <= daftarMenu; loop++) {
            MenuChickendiTampilkan[loop] = daftarMenuAyam.get(loop)[0] + " " + daftarMenuAyam.get(loop)[1];
        }
    }

    @Override
    public FlexMessage get() {

        Bubble Ayam = new PembuatanBubbleSpesifikMenu().createBubble(
                "https://i.ibb.co/9cjMnZb/brian-chan-12169-unsplash.jpg",
                "Ayam",
                "Pilih menu dengan ayam yang kamu inginkan....",
                MenuChickendiTampilkan
        );

        final Carousel carousel =
                Carousel.builder().contents(asList(Ayam)).build();
        return new FlexMessage(".Menuayam", carousel);
    }
}
