package com.bot.demo.CarouselPilihMenu.MenuUtama;

import com.bot.demo.CarouselPilihMenu.MencariDaftarResepMakanan;
import com.bot.demo.CarouselPilihMenu.PembuatanBubbleSpesifikMenu;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Carousel;
import org.json.JSONException;
import org.json.simple.parser.ParseException;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.function.Supplier;

import static java.util.Arrays.asList;

public class CarouselPilihUtamaDaging implements Supplier<FlexMessage> {
    String[] MenuBeefdiTampilkan = new String[5];

    public CarouselPilihUtamaDaging() throws JSONException, ParseException, URISyntaxException {
        ArrayList<String[]> daftarMenuDaging = new MencariDaftarResepMakanan().CariDenganAPI("Beef");
        
        for (int loop = 0; loop <= 4 ; loop++) {
            MenuBeefdiTampilkan[loop] = daftarMenuDaging.get(loop)[0] + " " + daftarMenuDaging.get(loop)[1];
        }
    }

    @Override
    public FlexMessage get() {

        Bubble Daging = new PembuatanBubbleSpesifikMenu().createBubble(
                "https://i.ibb.co/Mg71Fwq/patrick-kalkman-1442162-unsplash.jpg",
                "Daging",
                "Pilih menu dengan ikan yang kamu inginkan...",
                MenuBeefdiTampilkan
        );

        final Carousel carousel =
                Carousel.builder().contents(asList(Daging)).build();
        return new FlexMessage(".MenuDaging", carousel);
    }
}
