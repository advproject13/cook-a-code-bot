package com.bot.demo.CarouselPilihMenu.MenuUtama;

import com.bot.demo.CarouselPilihMenu.MencariDaftarResepMakanan;
import com.bot.demo.CarouselPilihMenu.PembuatanBubbleSpesifikMenu;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Carousel;
import org.json.JSONException;
import org.json.simple.parser.ParseException;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.function.Supplier;

import static java.util.Arrays.asList;

public class CarouselPilihUtamaIkan implements Supplier<FlexMessage> {
    String[] menuIkandiTampilkan = new String[5];

    public CarouselPilihUtamaIkan() throws JSONException, ParseException, URISyntaxException {
        ArrayList<String[]> daftarMenuIkan = new MencariDaftarResepMakanan().CariDenganAPI("Fish");

        for (int loop = 0; loop <= 4; loop++) {
            menuIkandiTampilkan[loop] = daftarMenuIkan.get(loop)[0] + " " + daftarMenuIkan.get(loop)[1];
        }
    }

    @Override
    public FlexMessage get() {

        Bubble Ikan = new PembuatanBubbleSpesifikMenu().createBubble(
                "https://i.ibb.co/XXXz0FL/gabriel-garcia-marengo-68299-unsplash.jpg",
                "Ikan",
                "Pilih menu dengan ikan yang kamu inginkan...",
                menuIkandiTampilkan
        );

        final Carousel carousel =
                Carousel.builder().contents(asList(Ikan)).build();
        return new FlexMessage(".MenuIkan", carousel);
    }
}
