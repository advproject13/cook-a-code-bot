package com.bot.demo.CarouselPilihMenu;


import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;


public class MencariDaftarResepMakanan {


    private String spoonAcularFormat = "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/food/menuItems/suggest?number=10&" + "query=%s";

    public ArrayList<String[]> CariDenganAPI (String bahanMakananan) throws JSONException, org.json.simple.parser.ParseException, URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();
        String AcularQuery = String.format(spoonAcularFormat, bahanMakananan);
        URI linkAPI = new URI(AcularQuery);

        HttpHeaders headers = new HttpHeaders();
        headers.set("X-RapidAPI-Host", "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com");
        headers.set("X-RapidAPI-Key", "f812552b29msha701e7fb5b69a14p11b1bfjsnaeb28c3581d5");


        HttpEntity requestEntity = new HttpEntity(null, headers);

        ResponseEntity<String> responseResult = restTemplate.exchange(linkAPI, HttpMethod.GET, requestEntity, String.class);

        JSONParser jsonParser = new JSONParser();
        JSONObject dataMenu = (JSONObject) jsonParser.parse(responseResult.getBody());

        JSONArray daftarResep = (JSONArray) dataMenu.get("results");
        ArrayList<String[]> daftarResepPilihan = new ArrayList<>();
        for(Object resep : daftarResep){
            JSONObject menu = (JSONObject) resep;
            String nama_resep = (String) menu.get("title");
            Long id_resep = (Long) menu.get("id");
            String[] uniqueMenuResep = new String[]{nama_resep, id_resep.toString()};
            daftarResepPilihan.add(uniqueMenuResep);
        }
        return daftarResepPilihan;
    }
}
