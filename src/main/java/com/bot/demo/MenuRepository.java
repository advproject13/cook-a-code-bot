package com.bot.demo;

import org.springframework.data.repository.CrudRepository;
import com.bot.demo.Menu;
import java.util.List;
import java.util.Optional;

public interface MenuRepository extends CrudRepository<Menu, Long> {
    Optional<Menu> findByNamaResep(String username);

    Optional<Menu> findByDeskripsi(String email);

    Optional<Menu> findByBahan(String email);

    Optional<Menu> findByInstruksi(String email);

    Optional<Menu> findByPorsi(String email);

    List<Menu> findAll();

    List<Menu> findAllByNamaResep(String namaResep);
}
