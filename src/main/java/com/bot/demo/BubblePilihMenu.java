package com.bot.demo;

import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.*;
import com.linecorp.bot.model.message.flex.component.Button.ButtonHeight;
import com.linecorp.bot.model.message.flex.component.Button.ButtonStyle;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Carousel;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import java.util.function.Supplier;

import static java.util.Arrays.asList;

public class BubblePilihMenu implements Supplier<FlexMessage> {

    @Override
    public FlexMessage get() {

        Bubble hidangan_Pembuka = createBubble(
                "https://i.ibb.co/MpNVwF1/Dessert.jpg",
                "Hidangan Pembuka",
                "Ayo pilih dessert ...",
                new String[]{"Kue", "Es Krim", "Puding"}

        );

        Bubble hidangan_Utama = createBubble(
                "https://i.ibb.co/ZgcY6MC/Main-Course.jpg",
                "Hidangan Utama",
                "Ayo pilih main course ...",
                new String[]{"Ayam", "Ikan", "Daging"}
        );

        Bubble hidangan_Penutup = createBubble(
                "https://i.ibb.co/tQz47vN/Appetizer.jpg",
                "Hidangan Penutup",
                "Ayo pilih appetizer ...",
                new String[]{"Buah", "Sayur", "Sup"}
        );
        final Carousel carousel =
                Carousel.builder().contents(asList(hidangan_Pembuka, hidangan_Utama, hidangan_Penutup)).build();
        return new FlexMessage("Pilih Menu", carousel);
    }

    private Bubble createBubble(String url, String menu, String deskripsi_Menu, String[] jenis_Menu){
        final Image heroBlock =
                Image.builder()
                        .url(url)
                        .size(Image.ImageSize.FULL_WIDTH)
                        .aspectRatio(Image.ImageAspectRatio.R20TO13)
                        .aspectMode(Image.ImageAspectMode.Cover)
                        .build();

        final Box bodyBlock = createBodyBlock(menu, deskripsi_Menu);
        final Box footerBlock = createFooterBlock(jenis_Menu, menu);

        return Bubble.builder()
                .hero(heroBlock)
                .body(bodyBlock)
                .footer(footerBlock)
                .build();
    }


    private Box createFooterBlock(String[] jenisMenu, String menu) {
        final Spacer spacer = Spacer.builder().size(FlexMarginSize.SM).build();
        final Separator separator = Separator.builder().build();
        final String[] splitDariMenu = menu.split(" ");

        final Button pilihMenu1 = Button
                .builder()
                .style(ButtonStyle.LINK)
                .height(ButtonHeight.SMALL)
                .action(new MessageAction(jenisMenu[0], splitDariMenu[1] + " " + jenisMenu[0]))
                .build();

        final Button pilihMenu2 =
                Button.builder()
                        .style(ButtonStyle.LINK)
                        .height(ButtonHeight.SMALL)
                        .action(new MessageAction(jenisMenu[1], splitDariMenu[1] + " " + jenisMenu[1]))
                        .build();

        final Button pilihMenu3 =
                Button.builder()
                        .style(ButtonStyle.LINK)
                        .height(ButtonHeight.SMALL)
                        .action(new MessageAction(jenisMenu[2], splitDariMenu[1] + " " + jenisMenu[2]))
                        .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.SM)
                .contents(asList(spacer,
                        separator,
                        pilihMenu1,
                        pilihMenu2,
                        pilihMenu3))
                .build();
    }

    private Box createBodyBlock(String menu, String deskripsi_Menu) {
        final Text title =
                Text.builder()
                        .text(menu)
                        .weight(TextWeight.BOLD)
                        .size(FlexFontSize.Md)
                        .build();
        final Text desc =
                Text.builder()
                        .text(deskripsi_Menu)
                        .weight(TextWeight.REGULAR)
                        .size(FlexFontSize.Md)
                        .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(asList(title, desc))
                .build();
    }
}