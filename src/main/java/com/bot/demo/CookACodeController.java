package com.bot.demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.response.BotApiResponse;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import org.springframework.beans.factory.annotation.Autowired;

@LineMessageHandler
public class CookACodeController {
    public static String token;
    public static String userId;
    @Autowired
    private LineMessagingClient lineMessagingClient;
    @Autowired
    private MenuRepository menuRepository;

    @EventMapping
    public void handleTextMessageEvent(MessageEvent<TextMessageContent> event) throws Exception {
        TextMessageContent message = event.getMessage();
        token = event.getReplyToken();
        userId = event.getSource().getUserId();
        handleTextContent(token, event, message);
    }

    private void handleTextContent(String replyToken, Event event, TextMessageContent content)
            throws Exception {

        String[] text = content.getText().split(" ");
        String[] tambah = content.getText().split("--");
        String menuDipilih;

        String userId = event.getSource().getUserId();
        switch (text[0]) {
            case ".help":
                this.reply(replyToken, new FlexMessageHelp().get());
                break;

            case ".pilih":
                List<Message> myList = Arrays.asList(new TextMessage("Hai, ingin pilih menu apa?"),
                        new BubblePilihMenu().get());
                this.reply(replyToken, myList);
                break;

            case ".rekomendasi":
                this.reply(replyToken, new RecommendReply().get());
                break;

            case ".tambah_resep":
                this.reply(replyToken, new TambahResep().get());
                break;

            case "Pembuka":
                menuDipilih = this.cekMenu(text);
                FlexMessage menuPembuka = new PilihMenu().pembuka(menuDipilih);
                this.reply(replyToken, menuPembuka);
                break;

            case "Utama":
                menuDipilih = this.cekMenu(text);
                FlexMessage menuUtama = new PilihMenu().utama(menuDipilih);
                this.reply(replyToken, menuUtama);
                break;

            case "Penutup":
                menuDipilih = this.cekMenu(text);
                FlexMessage menuPenutup = new PilihMenu().penutup(menuDipilih);
                this.reply(replyToken, menuPenutup);
                break;

            case ".Detail":
                String nama_Menu = this.cekMenu(text);
                    String proses = new TampilkanDetailPilihMenu().CariDetailMakanDenganAPI(text[-1]);
                    this.reply(replyToken, new TextMessage(proses));
                    break;


            case ".resep":
                if (tambah.length == 1 || tambah.length < 6) {
                    this.reply(replyToken,
                            new TextMessage("Kamu belum menuliskan semua yang dibutuhkan,"
                                    + " tolong lengkapi lagi ya!"));
                } else {
                    Optional<Menu> maybeMenu = menuRepository.findByNamaResep(tambah[1]);
                    if (maybeMenu.isPresent()) {
                        this.reply(replyToken,
                                new TextMessage("Nama Menu sudah pernah ditambahkan"));
                        break;
                    } else {
                        if (tambah.length < 6) {
                            this.reply(replyToken,
                                    new TextMessage("Isi semuanya untuk menambahkan menu baru"));
                            break;
                        }
                        // Throw data to database
                        Menu menu = new Menu(tambah[1], tambah[2], tambah[3], tambah[4],
                                Integer.parseInt(tambah[5]));
                        menuRepository.save(menu);
                        List<Message> resepTambah = Arrays.asList(new TextMessage(
                                "Selamat, Resep " + tambah[1] + " telah ditambahkan!"));
                        this.reply(replyToken, resepTambah);

                    }
                }
                break;

            case ".resep_favorit":
                this.reply(replyToken, new MenuFavoritController(userId).get());
                break;

            case ".tambah_favorit":
                break;

            case ".hapus_favorit":
                String[] resep = content.getText().split(" ");
                if (resep.length == 1) {
                    this.reply(replyToken, new TextMessage("Kamu belum menuliskan nama resepnya."));
                } else {
                    String out = resep[1];
                    for(int i = 2; i < resep.length ; i++) out += " " + resep[i];
                    if(resep[0] == ".tambah_favorit") {
                        this.reply(replyToken, new TextMessage(new MenuFavoritController(userId).tambahFavorit(out)));
                    } else {
                        this.reply(replyToken, new TextMessage(new MenuFavoritController(userId).hapusFavorit(out)));
                    }
                }
                break;

            default:
                this.reply(replyToken,
                        new TextMessage("Ketik .help untuk melihat fitur-fitur yang tersedia"));
        }
    }
    /*
     * Membalas pesan yang akan digabungkan menjadi satu kemudian dilempar pada method reply yang
     * akan merespon balasan
     */

    private void reply(String replyToken, Message message) {
        reply(replyToken, Collections.singletonList(message));
    }

    private void reply(String replyToken, List<Message> messages) {
        try {
            BotApiResponse apiResponse =
                    lineMessagingClient.replyMessage(new ReplyMessage(replyToken, messages)).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    /*
     * Membalas pesan dalam bentuk text
     */
    public void replyText(String replyToken, String message) {
        if (replyToken.isEmpty()) {
            throw new IllegalArgumentException("replyToken must not be empty");
        }
        if (message.length() > 1000) {
            message = message.substring(0, 1000 - 2) + "……";
        }
        this.reply(replyToken, new TextMessage(message));
    }

    private String cekMenu(String[] text) {
        ArrayList<String> inputUserArray = new ArrayList<String>();

        for (int looping = 1; looping < text.length; looping++) {
            inputUserArray.add(text[looping]);
        }
        String inputUser = String.join(" ", inputUserArray);
        return inputUser;
    }
}
