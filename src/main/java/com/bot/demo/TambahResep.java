package com.bot.demo;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;
import com.linecorp.bot.model.action.CameraAction;
import com.linecorp.bot.model.action.CameraRollAction;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.message.quickreply.QuickReply;
import com.linecorp.bot.model.message.quickreply.QuickReplyItem;

public class TambahResep implements Supplier<Message> {
    @Override
    public Message get() {
        final List<QuickReplyItem> items = Arrays.<QuickReplyItem>asList(
                QuickReplyItem.builder().action(CameraAction.withLabel("Take Photo")).build(),
                QuickReplyItem.builder().action(CameraRollAction.withLabel("Gallery")).build());

        final QuickReply quickReply = QuickReply.items(items);

        return TextMessage.builder().text("Format :\n"
                + ".resep --[Nama resep]--[Deskripsi singkat]--[Bahan yang diperlukan]--[Cara membuat]--[Jumlah porsi]")
                .quickReply(quickReply).build();
    }
}
