package com.bot.demo;

import com.linecorp.bot.model.action.*;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.message.quickreply.QuickReply;
import com.linecorp.bot.model.message.quickreply.QuickReplyItem;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class RecommendReply implements Supplier<Message> {

    @Override
    public Message get() {
        final List<QuickReplyItem> items = Arrays.<QuickReplyItem>asList(
                QuickReplyItem.builder()
                        .action(new MessageAction("Ayam", ".bahan_ayam"))
                        . build(),
                QuickReplyItem.builder()
                        .action(new MessageAction("Bakso", ".bahan_bakso"))
                        .build(),
                QuickReplyItem.builder()
                        .action(new MessageAction("Sayur", ".bahan_sayur"))
                        .build(),
                QuickReplyItem.builder()
                        .action(PostbackAction.builder()
                                .label("Sembunyikan")
                                .text("Berhasil!\n" +
                                        "Untuk melihat kata kunci ketik .rekomendasi")
                                .data("{PostbackAction: true}")
                                .build())
                        .build()
        );

        final QuickReply quickReply = QuickReply.items(items);

        return TextMessage
                .builder()
                .text("Pilih bahan dasar yang kamu inginkan!")
                .quickReply(quickReply)
                .build();
    }
}
