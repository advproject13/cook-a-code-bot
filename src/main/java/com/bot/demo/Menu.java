package com.bot.demo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Menu {
    @Id
    @Column(nullable = false, unique = true)
    private String namaResep;
    @Column(nullable = false)
    private String deskripsi;
    @Column(nullable = false)
    private String bahan;
    @Column(nullable = false)
    private String instruksi;
    @Column(nullable = false)
    private int porsi;

    public Menu() {
    }

    public Menu(String namaResep, String deskripsi, String bahan, String instruksi, Integer porsi) {
        setNamaResep(namaResep);
        setDeskripsi(deskripsi);
        setBahan(bahan);
        setInstruksi(instruksi);
        setPorsi(porsi);
    }

    public void setNamaResep(String namaResep) {
        this.namaResep = namaResep;
    }

    public String getNamaResep() {
        return namaResep;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setBahan(String bahan) {
        this.bahan = bahan;
    }

    public String getBahan() {
        return bahan;
    }

    public void setInstruksi(String instruksi) {
        this.instruksi = instruksi;
    }

    public String getInstruksi() {
        return instruksi;
    }

    public void setPorsi(int porsi) {
        this.porsi = porsi;
    }

    public int getPorsi() {
        return porsi;
    }
}
