package com.bot.demo;

import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.*;
import com.linecorp.bot.model.message.flex.component.Button.ButtonHeight;
import com.linecorp.bot.model.message.flex.component.Button.ButtonStyle;
import com.linecorp.bot.model.message.flex.component.Image.ImageAspectMode;
import com.linecorp.bot.model.message.flex.component.Image.ImageAspectRatio;
import com.linecorp.bot.model.message.flex.component.Image.ImageSize;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

import java.util.function.Supplier;

import static java.util.Arrays.asList;

public class FlexMessageHelp implements Supplier<FlexMessage> {
    @Override
    public FlexMessage get() {
        final Image heroBlock =
                Image.builder()
                        .url("https://i.ibb.co/7R8tPbp/image-2.jpg")
                        .size(ImageSize.FULL_WIDTH)
                        .aspectRatio(ImageAspectRatio.R20TO13)
                        .aspectMode(ImageAspectMode.Cover)
                        .build();

        final Box bodyBlock = createBodyBlock();
        final Box footerBlock = createFooterBlock();
        final Bubble bubble =
                Bubble.builder()
                        .hero(heroBlock)
                        .body(bodyBlock)
                        .footer(footerBlock)
                        .build();

        return new FlexMessage("HELP", bubble);
    }

    private Box createFooterBlock() {
        final Separator separator = Separator.builder().build();
        final Button pilihResepAction = Button
                .builder()
                .style(ButtonStyle.LINK)
                .height(ButtonHeight.SMALL)
                .action(new MessageAction("Pilih Resep", ".pilih"))
                .build();
        final Button tambahResepAction =
                Button.builder()
                        .style(ButtonStyle.LINK)
                        .height(ButtonHeight.SMALL)
                        .action(new MessageAction("Tambah Resep Baru", ".tambah_resep"))
                        .build();
        final Button resepFavAction =
                Button.builder()
                        .style(ButtonStyle.LINK)
                        .height(ButtonHeight.SMALL)
                        .action(new MessageAction("Resep Favorit", ".resep_favorit"))
                        .build();
        final Button rekomenAction =
                Button.builder()
                        .style(ButtonStyle.LINK)
                        .height(ButtonHeight.SMALL)
                        .action(new MessageAction("Rekomendasi Masakan", ".rekomendasi"))
                        .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.SM)
                .contents(asList(
                        separator,
                        pilihResepAction,
                        separator,
                        tambahResepAction,
                        separator,
                        resepFavAction,
                        separator,
                        rekomenAction))
                .build();
    }

    private Box createBodyBlock() {
        final Text title =
                Text.builder()
                        .text("Cook A Code")
                        .weight(TextWeight.BOLD)
                        .size(FlexFontSize.Md)
                        .build();
        final Text desc =
                Text.builder()
                        .text("Fitur yang tersedia")
                        .weight(TextWeight.REGULAR)
                        .size(FlexFontSize.Md)
                        .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(asList(title, desc))
                .build();
    }
}

