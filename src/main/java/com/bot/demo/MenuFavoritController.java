package com.bot.demo;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.*;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.Carousel;
import com.linecorp.bot.model.message.flex.unit.*;
import com.linecorp.bot.model.profile.UserProfileResponse;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;
import java.util.Optional;

import static java.util.Arrays.asList;

public class MenuFavoritController implements Supplier<FlexMessage> {
    //private final MessageEvent<TextMessageContent> event;
    //private UserProfileResponse userData;
    private String userId = "";

    @Autowired
    private MenuFavoritRepository menuRepository;
    @Autowired
    private MenuFavoritRepository menuFavoritRepository;

    public MenuFavoritController(String userId){
        this.userId = userId;
    }

    /*public MenuFavoritController(){
        *//*
         * LineMessagingCliend and UserProfileResponse
         * fungsi "buat ngambil userid"
         * sc : https://developers.line.biz/en/reference/messaging-api/#get-profile
         *//*
        *//*final LineMessagingClient client = LineMessagingClient
                .builder("<channel access token>")
                .build();

        final UserProfileResponse userProfileResponse;
        try {
            userProfileResponse = client.getProfile("<userId>").get();
            userId = client.getProfile("<userId>").get().getUserId(); //additional
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            //return null;
        }*//*
        userId = event.getSource().getUserId();
    }
    */

    @Override
    public FlexMessage get() {


        ArrayList<Bubble> dataFavorit = new ArrayList<>();
        getDataFavorit(userId, dataFavorit);

        final Carousel carousel =
                Carousel.builder().contents(dataFavorit).build();
        return new FlexMessage(".resep", carousel);
    }


    private Bubble createBubble(String url, String namaResep, String desc){
        final Image heroBlock =
                Image.builder()
                        .url(url)
                        .size(Image.ImageSize.FULL_WIDTH)
                        .aspectRatio(Image.ImageAspectRatio.R20TO13)
                        .aspectMode(Image.ImageAspectMode.Cover)
                        .build();

        final Box bodyBlock = createBodyBlock(namaResep, desc);
        final Box footerBlock = createFooterBlock(namaResep);

        return Bubble.builder()
                .hero(heroBlock)
                .body(bodyBlock)
                .footer(footerBlock)
                .build();
    }

    private Box createBodyBlock(String namaResep, String desc) {
        final Text title =
                Text.builder()
                        .text(namaResep)
                        .weight(Text.TextWeight.BOLD)
                        .size(FlexFontSize.XL)
                        .build();
        final Text description =
                Text.builder()
                        .text("Deskripsi")
                        .flex(3)
                        .size(FlexFontSize.SM)
                        .color("#AAAAAA")
                        .build();

        final Text customDesc =
                Text.builder()
                        .text(desc)
                        .size(FlexFontSize.SM)
                        .wrap(true)
                        .flex(5)
                        .gravity(FlexGravity.CENTER)
                        .color("#666666")
                        .build();

        final Box boxDesc = Box.builder()
                .layout(FlexLayout.BASELINE)
                .spacing(FlexMarginSize.SM)
                .contents(asList(description, customDesc))
                .build();

        final Box boxInside = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.SM)
                .margin(FlexMarginSize.LG)
                .contents(asList(boxDesc))
                .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(asList(title,
                        boxInside))
                .build();
    }

    private Box createFooterBlock(String namaResep) {
        final Separator separator = Separator.builder().build();
        final Button footer1 =
                Button.builder()
                        .style(Button.ButtonStyle.LINK)
                        .gravity(FlexGravity.TOP)
                        .height(Button.ButtonHeight.SMALL)
                        .flex(1)
                        .action(new MessageAction("Lihat Resep?", namaResep))
                        .build();
        final Button footer2 =
                Button.builder()
                        .style(Button.ButtonStyle.LINK)
                        .gravity(FlexGravity.TOP)
                        .height(Button.ButtonHeight.SMALL)
                        .flex(1)
                        .action(new MessageAction("Hapus Favorit", ".hapus_favorit " + namaResep))
                        .build();
        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.SM)
                .flex(1)
                .contents(asList(
                        separator,
                        footer1, footer2))
                .build();
    }

    private void getDataFavorit(String userId, ArrayList<Bubble> dataFavorit){
        try {
            List<MenuFavorit> menuFavoritList = menuFavoritRepository.findAllByUserId(userId);

            for (int i = 0; i < menuFavoritList.size(); i++) {
                Menu tempMenu = menuRepository.findAllByNamaResep(menuFavoritList.get(i).getNamaResep());
                Bubble tempBubble = createBubble("https://i.ibb.co/XYsbLmP/photo-1455619452474-d2be8b1e70cd.jpg",
                        tempMenu.getNamaResep(), tempMenu.getDeskripsi());

                dataFavorit.add(tempBubble);
            }
        }
        catch (NullPointerException e) {
            Bubble tempBubble = createBubble(
                    "https://i.ibb.co/XYsbLmP/photo-1455619452474-d2be8b1e70cd.jpg",
                    "Opor Ayam",
                    "Sajian ayam lezat yang dihidangkan dengan kuah santan yang sesuai " +
                            "dengan lidah orang Indonesia");
            dataFavorit.add(tempBubble);
        }

    }

    public String tambahFavorit(String namaResep) throws NullPointerException {
        String mesg = "Error! -tambahFavorit ";
        try {
            /*Optional<Menu> optionalMenu = menuRepository.findByNamaResep(namaResep);

            if(!optionalMenu.isPresent()){
                return namaResep + " tidak ditemukan";
            }*/
            mesg += " ok awal ";
            mesg += " " + userId + " ";
            Optional<MenuFavorit> optionalMenuFavorit =
                    menuFavoritRepository.findByUserIdAndNamaResep(userId, namaResep);

            mesg += " ok1 ";
            if(optionalMenuFavorit.isPresent()){
                return namaResep + " sudah terdaftar di dalam daftar favoritmu.";
            }

            mesg += " ok2 ";

            MenuFavorit menuFavorit = new MenuFavorit(userId, namaResep);
            menuFavoritRepository.save(menuFavorit);

            return namaResep + " sukses dihapus dari daftar favoritmu.";
        }
        catch (Exception e) {
            return mesg + e;
        }
    }

    public String hapusFavorit(String namaResep) {
        try {
            /*Optional<Menu> optionalMenu = menuRepository.findByNamaResep(namaResep);

            if(!optionalMenu.isPresent()){
                return namaResep + " tidak ditemukan";
            }*/

            Optional<MenuFavorit> optionalMenuFavorit =
                    menuFavoritRepository.findByUserIdAndNamaResep(userId, namaResep);

            if(!optionalMenuFavorit.isPresent()){
                return namaResep + " tidak ditemukan dari daftar favoritmu.";
            }

            MenuFavorit menuFavorit = new MenuFavorit(userId, namaResep);
            menuFavoritRepository.delete(menuFavorit);

            return namaResep + " sukses dihapus dari daftar favoritmu.";
        }
        catch (Exception e) {
            return "Error! -hapusFavorit" + e;
        }
    }

    /*private boolean resepIsExist(String namaResep) {
        Optional<Menu> optionalMenu = menuRepository.findByNamaResep(namaResep);

        if(!optionalMenu.isPresent()){
            return false;
        }

        return true;
    }*/
}