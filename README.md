```bash
Kelompok    : 13
Kelas       : ADPRO C
Anggota     : Jeremy                        1706039742
              Dzaky Noor Hasyim             1706021606
              Falya Aqiela Sekardina        1706027673
              Ignatius Bagussuputra         1706039774
              Karina Ivana                  1706979335


```

# Line Chatbot &bull; [![Pipeline](https://gitlab.com/advproject13/cook-a-code-bot/badges/master/pipeline.svg)](https://gitlab.com/advproject13/cook-a-code-bot/commits/master)&nbsp; [![coverage report](https://gitlab.com/advproject13/cook-a-code-bot/badges/master/coverage.svg)](https://gitlab.com/advproject13/cook-a-code-bot/commits/master)
add line : `@029rcttz`

# Pembagian Tugas

Tugas Pemrograman Kelompok 13<br>

Jeremy :<br>
Dzaky Noor Hasyim :<br>
Falya Aqiela Sekardina :<br>
Ignatius Bagussuputra :<br>
Karina Ivana :<br>

# Development

Generate code coverage report

```
gradle jacocoTestReport
```

Verifies code coverage metrics

```
gradle jacocoTestCoverageVerification
```
